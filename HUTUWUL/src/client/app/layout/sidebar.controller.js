(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller('SidebarController', SidebarController);

  SidebarController.$inject = ['$state', 'routerHelper'];
  /* @ngInject */
  function SidebarController($state, routerHelper) {
    var vm = this;
    var states = routerHelper.getStates();
    vm.isCurrent = isCurrent;
    vm.color = "";
    vm.TabTitle = "";

    activate();


    function activate() { getNavRoutes(); }

    function getNavRoutes() {
      vm.navRoutes = states.filter(function(r) {
        return r.settings && r.settings.nav;
      }).sort(function(r1, r2) {
        return r1.settings.nav - r2.settings.nav;
      });
    }

    function isCurrent(route) {
      if (!route.title || !$state.current || !$state.current.title) {
        return '';
      }else{
       // console.log(route.title + " - " + $state.current + " - " + $state.current.title)
      }
      var menuName = route.title;
      console.log(menuName);
      return $state.current.title.substr(0, menuName.length) === menuName ? 'current' : '';
    }

   // vm.test = function (r){
   //   console.log(r);
   // }

    vm.titleColor = function(r){
      vm.TabTitle = r.title;
      console.log(r.title);
    }

  }
})();
