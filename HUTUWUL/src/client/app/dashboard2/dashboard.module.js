(function() {
  'use strict';

  angular.module('app.dashboard2', [
    'app.core',
    'app.widgets'
  ]);
})();
