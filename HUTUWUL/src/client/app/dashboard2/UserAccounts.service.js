/**
 * Created by Matt on 3/2/2017.
 */


angular
  .module('app.dashboard2')
  .service('UserAccounts',  ['$http','$q', function($http,$q){

    var HttpData = function() {
      var deffered = $q.defer();
      $http.get("ditdutdit.json")
        .then(function (response) {
          deffered.resolve(response.data);
        });

      return deffered.promise;

    };

    return{

      HttpData : HttpData
    }
  }]);
