(function() {
  'use strict';

  angular
    .module('app.dashboard2')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'dashboard2',
        config: {
          url: '/aa',
          templateUrl: 'app/dashboard2/dashboard.html',
          controller: 'DashboardController2',
          controllerAs: 'vm',
          title: 'dashboard2',
          settings: {
            nav: 2,
            content: '<i class="fa fa-dashboard"></i> User List'
          }
        }
      }
    ];
  }
})();
