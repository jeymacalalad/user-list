(function() {
  'use strict';

  angular
    .module('app.dashboard2')
    .controller('DashboardController2', DashboardController2);
   // .service('DataGet', DataGet);

  DashboardController2.$inject = ['$http','UserAccounts','logger','$scope'];
  /* @ngInject */
  function DashboardController2($http,UserAccounts,logger,$scope) {
    var vm = this;

    UserAccounts.HttpData().then(function(data){
      vm.haha = data;
    });

    vm.Searchby = "";
    vm.config = {
      itemsPerPage: 5,
      fillLastPage: true
    }



    vm.hey = function(r){
      console.log(r);
    }


    vm.editAccount = function(){
      $("#EditUserModal").modal("hide")
      if(vm.picChanged){
        vm.haha[vm.index].userImage = vm.newPic;
      }
      vm.haha[vm.index].firstName = vm.Fname;
      vm.haha[vm.index].middleName = vm.Mname;
      vm.haha[vm.index].lastName = vm.Lname;
      vm.haha[vm.index].email = vm.email;
      vm.haha[vm.index].contactNumber = vm.cNum;
      vm.haha[vm.index].password = vm.cPass;
      vm.haha[vm.index].birthDate = vm.Bday;
      vm.haha[vm.index].country = document.getElementById('country').value;
      vm.haha[vm.index].address = vm.Add;
      vm.haha[vm.index].gender = document.getElementById('gender').value;
      vm.haha[vm.index].permission = [document.getElementById('ePOS').checked, document.getElementById('eInventory').checked, document.getElementById('eSales').checked]
    }

    vm.addAccount = function(){
      vm.haha.splice(1,0,{"firstName": vm.Fname,
        "middleName": vm.Mname,
        "lastName": vm.Lname,
        "email": vm.email,
        "contactNumber": vm.cNum,
        "password": vm.cPass,
        "birthDate": vm.Bday,
        "country": document.getElementById('countryReg').value,
        "address": vm.Add,
        "gender": document.getElementById('genderReg').value,
        "permission":[document.getElementById('aPOS').checked, document.getElementById('aInventory').checked, document.getElementById('aSales').checked]});
    }

    vm.removeAccount = function(){
      vm.haha.splice(vm.index,1);
      $("#EditUserModal").modal("hide")
    }

    vm.collectData = function(index,p){
      vm.checkchanged = false;
      vm.picChanged = false;
      vm.UserImage = p.userImage;
      console.log(p.gender)
      document.getElementById('country').value= p.country;
      document.getElementById('gender').value= p.gender;
      vm.Fname = p.firstName;
      vm.Lname = p.lastName;
      vm.Mname = p.middleName;
      vm.email = p.email;
      vm.cNum = p.contactNumber ;
      vm.Pass = p.password;
      vm.cPass = p.password;
      vm.Bday = new Date(p.birthDate);
      vm.Gender = p.gender;
      vm.Add = p.address;
      vm.Country = p.country;
      vm.index = index;
      document.getElementById('ePOS').checked = p.permission[0];
      document.getElementById('eInventory').checked = p.permission[1];
      document.getElementById('eSales').checked = p.permission[2];

      vm.myEditForm.$setPristine();
    }

    vm.DefaultImg = "../../images/Accounts/default.png";

    vm.clearFields = function(){
      vm.UserImage = vm.DefaultImg;
      vm.myForm.$setPristine();
      vm.myForm.$setUntouched();
      vm.Fname = "";
      vm.Lname = "";
      vm.Mname = "";
      vm.email = "";
      vm.cNum = "" ;
      vm.Pass = "";
      vm.cPass = "";
      vm.Bday = new Date("00/00/0000");
      vm.Gender = "";
      vm.Country = "";
      vm.Add = "";
      document.getElementById('aPOS').checked = false;
      document.getElementById('aInventory').checked = false;
      document.getElementById('aSales').checked = false;
    }


    vm.checkPass = function(id) {
      if(vm.cPass != "" ){
      if(vm.Pass == vm.cPass){
        document.getElementById(id).style.display = "none";
      }else{
        vm.status = "hidden";
        document.getElementById(id).style.display = "block";
      }
      }else{
        document.getElementById(id).style.display = "none";
      }
    }

    vm.clearCP = function(){
      vm.cPass = "";
    }

    vm.LogInEmail ="";
    vm.LogInPass ="";
    vm.errorMsg ="";

    vm.LogIn = function(){
      for(var x=0; x < vm.haha.length;x++){
        if(vm.LogInEmail == vm.haha[x].email){
          if(vm.LogInPass == vm.haha[x].password) {
            $("#LogIn").modal("hide")
            document.getElementById('LogInError').style.display = "none";
          }else{
            document.getElementById('LogInError').style.display = "block";
            vm.errorMsg = "Wrong password";
          }
          break;
        }else{
          document.getElementById('LogInError').style.display = "block";
          vm.errorMsg = "Email not found";
        }
      }
    }

    vm.permission = function(p, num){
      if(p.permission[num] == true){
        return "";
      }else{
        return "lightgray";
      }
    }

    vm.checkchanged = false;

    vm.CheckboxVC = function() {
      vm.checkchanged = true;
    }

/*    vm.erroo = "";
    vm.validmsg = function(){
      if(($scope.vm.myForm.firstName.$dirty || $scope.vm.myForm.firstName.$touched) && $scope.vm.myForm.firstName.$invalid){
        vm.erroo = "Invalid user name";
      }else{
        vm.erroo = "";
      }
      console.log(vm.erroo);
    };*/

    vm.imageOpacity = "100%";
    vm.ImageEnter = function(){
      vm.imageOpacity = "0.5";
    }
    vm.ImageLeave = function(){
      vm.imageOpacity = "1";
    }


    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
    });

    vm.picChanged = false;

    $('#changePic').change( function(event) {
      var tmppath = URL.createObjectURL(event.target.files[0]);
      $('#editImage').attr('src',URL.createObjectURL(event.target.files[0]));
      vm.picChanged = true;
      console.log(vm.picChanged);
      vm.newPic = tmppath;
    });



  }
})();
